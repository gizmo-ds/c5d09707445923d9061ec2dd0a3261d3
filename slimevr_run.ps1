# Copyright (c) 2022 Gizmo <cunyu@liuli.lol>

function Fail($message) {
    Write-Output $message
    [Console]::Readkey() | Out-Null
    Exit 1
}
$path = (Get-ChildItem -Name -Filter "*jre*"),(Get-ChildItem -Name -Filter "*jdk*") -ne $null
if($path.Count -eq 0) {Fail("当前目录下未找到JDK或JRE的文件夹")}
$path = '{0}\{1}\bin' -f ((Get-Location).Path,$path[0])
if((Test-Path "$path\java.exe") -eq $false) {Fail("Java目录下未找到bin文件夹")}
$env:Path+= ';' + $path
if((Test-Path "$((Get-Location).Path)\slimevr.jar") -eq $false) {Fail("当前目录下未找到slimevr.jar文件")}
java -Xmx512M -jar slimevr.jar
